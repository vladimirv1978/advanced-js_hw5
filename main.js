let containerBlog = document.querySelector(".container-blog");

class Card {
  constructor(name, title, post, email, idPost) {
    this.name = name;
    this.title = title;
    this.post = post;
    this.email = email;
    this.idPost = idPost;
  }

  render() {
    let cardWrapper = document.createElement("div");
    cardWrapper.className = "card-wrapper";
    cardWrapper.innerHTML = `<span class="user-name">${this.name}</span> <br>
    <span class="user-title">${this.title}</span> <br>
    <span class="user-post">${this.post}</span> <br>
    <span class="user-email">${this.email}</span> <br>
    <button type="button" class="blog-button-delete">Delete</button>`;
    containerBlog.prepend(cardWrapper);

    let deleteButton = document.querySelector(".blog-button-delete");
    this.addListnerButtonDelete(deleteButton, cardWrapper);
  }
  
  addListnerButtonDelete(deleteButton, cardWrapper){
    deleteButton.addEventListener("click", () => {
      this.deletePost(cardWrapper, this.idPost);
    });
  }

  deletePost(cardWrapper, idPost) {
    fetch(`https://ajax.test-danit.com/api/json/posts/${idPost}`, {
      method: "DELETE",
    }).then((res) => console.log("Ответ", res));
    cardWrapper.remove();
  }
}

function gettingUserData() {
  fetch("https://ajax.test-danit.com/api/json/users")
    .then((res) => res.json())
    .then((users) => {
      fetch(`https://ajax.test-danit.com/api/json/posts`, {
        method: "GET",
      })
        .then((res) => res.json())
        .then((dataPost) => {
          let nameUser;
          for (let i = 0; i <= users.length - 1; i++) {
            for (let j = 0; j <= dataPost.length - 1; j++) {
              if (users[i].id === dataPost[j].userId) {
                nameUser = users[i].name;
                const userCard = new Card(
                  nameUser,
                  dataPost[j].title,
                  dataPost[j].body,
                  users[i].email,
                  dataPost[j].id
                );
                userCard.render();
              }
            }
          }
        });
    });
}

gettingUserData();
